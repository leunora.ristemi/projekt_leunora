package ListTesting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;
import java.util.TreeSet;

public class ListTestingModel {
	protected enum LIST_TYPES {ArrayList, LinkedList, TreeSet, HashSet};
	
	/**
	 * Run the defined test, returning the total elapsed time in seconds
	 * @param amountOfData The amount of elements to add to the list
	 * @param listType The type of list to use
	 * @param whereChoice Where to insert the elements in the list
	 * @return the elapsed time in seconds, as a float
	 */
	public float runTest(Integer amountOfData, LIST_TYPES listType) {
		// Create the data objects in an array
		SampleData[] data = createData(amountOfData);		
		Collection<SampleData> collection = createCollection(listType);
		
		// Perform the test
		long startTime = System.currentTimeMillis();
		addDataToCollection(data, collection);
		long endTime = System.currentTimeMillis();
		return (endTime - startTime) / 1000.0f;
	}
	
	public float runFindTest(Integer amountOfData, LIST_TYPES listType) {
		// data objects in an array
		SampleData[] data = createData(1000000);
		Collection<SampleData> collection = createCollection(listType);
		
		//data in the collection
		addDataToCollection(data, collection);
		
		//output
		long startTime = System.currentTimeMillis();
		findDataCollection(data, collection, amountOfData);
		long endTime = System.currentTimeMillis();
		return (endTime-startTime) / 10000.0f;
		
	}

	private SampleData[] createData(Integer amountOfData) {
		SampleData[] data = new SampleData[amountOfData];
		for (int i = 0; i < amountOfData; i++) data[i] = new SampleData();
		return data;
	}
	
	private Collection<SampleData> createCollection(LIST_TYPES listType) {
		// Create an empty list of the desired type
		Collection<SampleData> collection = null;
		switch(listType) {
		case ArrayList:
			collection = new ArrayList<>();
			break;
		case LinkedList:
			collection = new LinkedList<>();
			break;
		case TreeSet:
			collection = new TreeSet<>(Comparator.comparingInt(SampleData::getID));
			break;
		case HashSet:
			collection = new HashSet<>();
			break;
		}
		
		return collection;
	}
	
	private void addDataToCollection(SampleData[] data, Collection<SampleData> collection) {
		for (SampleData element : data) {
			collection.add(element);
		}
	}
	
	private void findDataCollection(SampleData[] data, Collection<SampleData> collection, Integer amountOfData) {
		Random rand = new Random();
		for(int i = 0; i < amountOfData; i++) {
			SampleData element = data[rand.nextInt(data.length)];
			collection.contains(element);
		}
	}
}

