package ListTesting;

import java.text.DecimalFormat;

import ListTesting.ListTestingModel.LIST_TYPES;
import javafx.event.ActionEvent;

public class ListTestingController {
	private ListTestingModel model;
	private ListTestingView view;

	private final DecimalFormat timeFormatter = new DecimalFormat("0.000");
	
	public ListTestingController(ListTestingModel model, ListTestingView view) {
		this.model = model;
		this.view = view;
		
		view.btnGo.setOnAction(this::setUpTest);
	}
	
	private void setUpTest(ActionEvent e) {
		// Get selections from View
		Integer amountOfData = view.cmbNumElements.getValue();
		LIST_TYPES listType = view.cmbListType.getValue();
		String testOp = view.cmbTestOp.getValue();

		float runTime = 0.0f;
		
		if(testOp.equals("Add elements")) {
			runTime = model.runTest(amountOfData, listType);
		}else if(testOp.equals("Find elements")){
			runTime = model.runFindTest(amountOfData, listType);
		}
		
		view.lblResult.setText("Time: " + timeFormatter.format(runTime) + " seconds");
		
		// Trigger garbage collection
		System.gc();
	}

}

