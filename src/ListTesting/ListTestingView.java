package ListTesting;

import ListTesting.ListTestingModel.LIST_TYPES;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ListTestingView {
	private Stage stage;
	private ListTestingModel model;

	protected final Integer[] DATA_AMOUNTS = {1000, 3000, 10000,30000,100000,300000,1000000};

	private Label lblNumElements = new Label("Amount of data");
	protected ComboBox<Integer> cmbNumElements = new ComboBox<>();
	private Label lblListType = new Label("List type");
	protected ComboBox<LIST_TYPES> cmbListType = new ComboBox<>();
	protected Button btnGo = new Button("Run test");
	protected Label lblResult = new Label();
	private Label lbltestOp = new Label("Test Operation");
	protected ComboBox<String> cmbTestOp = new ComboBox();
	
	
	public ListTestingView(Stage stage, ListTestingModel model) {
		this.stage = stage;
		this.model = model;

		cmbNumElements.getItems().setAll(DATA_AMOUNTS);
		cmbNumElements.setValue(DATA_AMOUNTS[0]);
		
		cmbListType.getItems().setAll(LIST_TYPES.values());
		cmbListType.setValue(LIST_TYPES.ArrayList);
		
		cmbTestOp.getItems().addAll("Add elements" , "Find elements");
		cmbTestOp.setValue("Add elements");
		
		GridPane grid = new GridPane();
		grid.addRow(0, lblNumElements, cmbNumElements);
		grid.addRow(1, lblListType, cmbListType);
		grid.addRow(2, lbltestOp, cmbTestOp);
		
		VBox root = new VBox(grid, btnGo, lblResult);
		root.getStyleClass().add("vbox");

        Scene scene = new Scene(root);
		scene.getStylesheets().add(
				getClass().getResource("styles.css").toExternalForm());        
        stage.setScene(scene);
        stage.setTitle("List testing");
	}

	public void start() {
		stage.show();
	}

}

