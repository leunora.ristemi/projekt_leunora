package singlyLinkedList;

import singlyLinkedList.ListException.ListErrorCode;

public class SinglyLinkedList<T> {
	private ListElement<T> head = null;

	public boolean isEmpty() {
		return (head == null);
	}

	public void add(int position, T o) throws ListException {
		if(position < 0) throw new ListException(ListException.ListErrorCode.PositionTooSmall);
		ListElement<T> newElement = new ListElement<>(o);
		if(position == 0) {
			newElement.next = head;
			head = newElement;
			return;
		}
		
		ListElement<T> cursor = head;
		for(int pos = 0; pos < position -1; pos++) {
			if(cursor == null) throw new ListException(ListException.ListErrorCode.PositionTooLarge);
			cursor = cursor.next;
		}
		if(cursor == null) throw new ListException(ListException.ListErrorCode.PositionTooLarge);
		newElement.next = cursor.next;
		cursor.next = newElement;
		}

	public T get(int position) throws ListException {
		if (position < 0) throw new ListException(ListErrorCode.PositionTooSmall);
		ListElement<T> cursor = head;
		for (int pos = 0; pos < position; pos++) {
			if (cursor == null) throw new ListException(ListErrorCode.PositionTooLarge);
			cursor = cursor.next;
		}
		if (cursor == null) throw new ListException(ListErrorCode.PositionTooLarge);
		return cursor.getData();
	}

	public T remove(int position) throws ListException {
		if (position < 0) throw new ListException(ListException.ListErrorCode.PositionTooSmall);
		if (head == null) throw new ListException(ListException.ListErrorCode.PositionTooLarge);
		if (position == 0) {
			T data = head.getData();
			head = head.next;
			return data;
		}
		
		ListElement<T> cursor = head;
		for(int pos=0; pos< position -1; pos++ ) {
			if(cursor == null ||cursor.next == null) throw new ListException(ListException.ListErrorCode.PositionTooLarge);
			cursor = cursor.next;
		}
		
		if(cursor.next == null) throw new ListException(ListException.ListErrorCode.PositionTooLarge);
		T data = cursor.next.getData();
		cursor.next=cursor.next.next;
		
		return data;
	}
	
	public int size() {
		int size = 0;
		ListElement <T> cursor = head;
		while(cursor != null) {
			size++;
			cursor = cursor.next;
		}
		return size;
	}
	
	public boolean contains(T elt) {
		ListElement<T> cursor = head;
		while(cursor != null) {
			if((elt == null && cursor.getData() == null ) || (elt != null && elt.equals(cursor.getData())))
				return true;
		}
		return false;
	}
}

