package JUnit;

public class WebAddressValidator {
	
	public boolean isValidP(String newValue) {
        boolean valid = true;

        try {
            int value = Integer.parseInt(newValue);
            if (value < 1 || value > 65535) valid = false;
        } catch (NumberFormatException e) {
            // wasn't an integer
            valid = false;
        }

        return valid;
    }

    public boolean isValidWB(String newValue) {
        boolean valid = false;
        String[] parts = newValue.split("\\.", -1);

        if (parts.length == 4) {
            valid = true;
            for (String part : parts) {
                try {
                    int value = Integer.parseInt(part);
                    if (value < 0 || value > 255) valid = false;
                } catch (NumberFormatException e) {
                    valid = false;
                }
            }
        }

        return valid;
    }
}
