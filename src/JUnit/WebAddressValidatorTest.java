package JUnit;

import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;

public class WebAddressValidatorTest {
	
	private WebAddressValidator validator = new WebAddressValidator();
	
	@Test
    public void testValidPortNumbers() {
        assertTrue(validator.isValidP("1"));
        assertTrue(validator.isValidP("80"));
        assertTrue(validator.isValidP("65535"));
    }

    @Test
    public void testInvalidPortNumbers() {
        assertFalse(validator.isValidP("0"));
        assertFalse(validator.isValidP("65536"));
        assertFalse(validator.isValidP("abc"));
        assertFalse(validator.isValidP("-1"));
    }

    @Test
    public void testValidWebAddresses() {
        assertTrue(validator.isValidWB("192.168.0.1"));
        assertTrue(validator.isValidWB("www.example.com"));
        assertTrue(validator.isValidWB("example.co"));
    }

    @Test
    public void testInvalidWebAddresses() {
        assertFalse(validator.isValidWB("256.256.256.256"));
        assertFalse(validator.isValidWB("192.168.0."));
        assertFalse(validator.isValidWB("example.c"));
        assertFalse(validator.isValidWB("exa.mple.com"));
    }
}


	    