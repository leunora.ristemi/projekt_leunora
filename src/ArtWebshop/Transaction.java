package ArtWebshop;

import java.time.LocalDate;
import java.util.ArrayList;

public class Transaction {
    private static int nextID = 0;
    private int idTransaction;
    private LocalDate date;
    private Customer customer;
    private ArrayList<Product> products;
    private double amount;

    // Constructor
    public Transaction(int idTransaction, LocalDate date, Customer customer, ArrayList<Product> products) {
        this.idTransaction = idTransaction;
        this.date = date;
        this.customer = customer;
        this.products = products;
        this.amount = calculateTotalAmount();
    }

    public static int getNextID() {
        return nextID++;
    }
    
    private double calculateTotalAmount() {
        double total = 0.0;
        for (Product product : products) {
            total += product.getPrice();
        }
        return total;
    }

    @Override
    public String toString() {
        return "Transaction id: " + idTransaction + ", date: " + date + ", customer: " + customer.getFirstName() + " " + customer.getLastName() + ", products: " + products.size() + ", amount: " + amount;
    }
}

