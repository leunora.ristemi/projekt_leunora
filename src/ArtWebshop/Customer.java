package ArtWebshop;

import java.util.ArrayList;

public class Customer {
	
	private static int nextID = 0;
	private final int idCustomer;
	private String firstName;
	private String lastName;
	private Address billingAddress;
	private Address deliveryAddress;
	private ArrayList<Transaction> transactions;
	
	private static int getNextID() {
		return nextID++;
	}
	
	//Constructor
	public Customer(String firstName, String lastName) {
		this.idCustomer = getNextID();
        this.firstName = firstName;
        this.lastName = lastName;
        this.transactions = new ArrayList<>();
	}
	
	public int getIdCustomer() {
		return idCustomer;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Address getBillingAdress() {
		return billingAddress;
	}

	public void setBillingAdress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}

	public Address getDeliveryAdress() {
		return deliveryAddress;
	}

	public void setDeliveryAdress(Address deliveryAdress) {
		this.deliveryAddress = deliveryAddress;
	}

	public ArrayList<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(ArrayList<Transaction> transactions) {
		this.transactions = transactions;
	}
	
	
}
