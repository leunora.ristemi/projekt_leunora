package ArtWebshop;

public class Painting extends Product{
	
	public Painting(int idProduct, String name, double price) {
        super(idProduct, name, price);
    }
	
	private Medium medium; 
	private boolean authentic;
	
	//Getters and Setters
	public Medium getMedium() {
		return medium;
	}
	public void setMedium(Medium medium) {
		this.medium = medium;
	}
	public boolean isAuthentic() {
		return authentic;
	}
	public void setAuthentic(boolean authentic) {
		this.authentic = authentic;
	}

}
