package ArtWebshop;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class ArtWebshopMain {
    private ArrayList<Customer> customers;
    private ArrayList<Transaction> transactions;
    private ArrayList<Product> products;

    public ArtWebshopMain() {
        customers = new ArrayList<>();
        transactions = new ArrayList<>();
        products = new ArrayList<>();
    }

    public static void main(String[] args) {
        ArtWebshopMain webshop = new ArtWebshopMain();
        webshop.run();
    }

    public void run() {
        Scanner in = new Scanner(System.in);

        // Test Data
        Product p1 = new Painting(1, "Sunset", 150.0);
        Product sc1 = new Sculpture(2, "The Thinker", 300.0);
        products.add(p1);
        products.add(sc1);

        System.out.println("Welcome to our Art Webshop!");
        boolean running = true;

        while (running) {
            System.out.println("Please choose an option:");
            System.out.println("1. Register as a new customer");
            System.out.println("2. Register a new product");
            System.out.println("3. Make a purchase");
            System.out.println("4. List all transactions");
            System.out.println("5. Exit");

            int choice = in.nextInt();
            in.nextLine();  

            switch (choice) {
                case 1:
                    registerCustomer(in);
                    break;
                case 2:
                    registerProduct(in);
                    break;
                case 3:
                    buyProduct(in);
                    break;
                case 4:
                    listTransactions();
                    break;
                case 5:
                    running = false;
                    break;
                default:
                    System.out.println("Invalid option. Please try again.");
            }
        }
        in.close();
    }
    
    private void registerCustomer(Scanner in) {
        System.out.println("Enter your first name:");
        String firstName = in.nextLine();
        System.out.println("Enter your last name:");
        String lastName = in.nextLine();
        System.out.println("Enter your billing address, \n"
        		+ "you must wrap when adding the street, city and postcode");
        Address billingAddress = new Address();
        billingAddress.setStreet(in.nextLine());
        billingAddress.setCity(in.nextLine());
        billingAddress.setPostCode(in.nextInt());
        
        in.nextLine();  
        
        System.out.println("Enter your delivery address, \n"
        		+ "you must wrap when adding the street, city and postcode");
        Address deliveryAddress = new Address();
        deliveryAddress.setStreet(in.nextLine());
        deliveryAddress.setCity(in.nextLine());
        deliveryAddress.setPostCode(in.nextInt());
        in.nextLine();  

        Customer newCustomer = new Customer(firstName, lastName);
        newCustomer.setBillingAdress(billingAddress);
        newCustomer.setDeliveryAdress(deliveryAddress);
        customers.add(newCustomer);

        System.out.println("Customer registered successfully! Your Customer ID is: " + newCustomer.getIdCustomer());
    }
    
    private void registerProduct(Scanner in) {
    	listProducts();
        System.out.println("Enter the ID of your new product:");
        int idProduct = in.nextInt();
        in.nextLine(); 
        
        System.out.println("Enter the name of your product:");
        String name = in.nextLine();
        System.out.println("Enter the price of your product:");
        double price = in.nextDouble();
        in.nextLine();  

        System.out.println("Is this product a Painting (1) or a Sculpture (2)?");
        int type = in.nextInt();
        in.nextLine(); 

        if (type == 1) {
            Painting painting = new Painting(idProduct, name, price);
            System.out.println("Enter the medium (Watercolor or Oil) \n"
            		+ "make sure you capitalise the first letter ");
            String medium = in.nextLine();
            painting.setMedium(Medium.valueOf(medium));
            System.out.println("Is it authentic?");
            boolean authentic = getBooleanInput(in);
            painting.setAuthentic(authentic);
            products.add(painting);
            
        } else if (type == 2) {
        	
            Sculpture sculpture = new Sculpture(idProduct, name, price);
            System.out.println("Enter the material (Wood, Clay, Bronze):");
            String material = in.nextLine();
            sculpture.setMaterial(Material.valueOf(material));
            System.out.println("Is it original?");
            boolean isOriginal = getBooleanInput(in);
            sculpture.setOriginal(isOriginal);
            products.add(sculpture);
        } else {
            System.out.println("Invalid product type.");
        }

        System.out.println("Product registered successfully!");
    }
    
    private boolean getBooleanInput(Scanner in) {
        System.out.println("Enter 'yes' or 'no':");
        String input = in.nextLine();
        return input.equalsIgnoreCase("yes");
    }
    
    private void buyProduct(Scanner in) {
    	System.out.println("You can buy one of these products: ");
    	listProducts();
        System.out.println("To buy something, enter your customer ID:");
        int customerId = in.nextInt();
        in.nextLine();  

        Customer customer = null;
        for (Customer c : customers) {
            if (c.getIdCustomer() == customerId) {
                customer = c;
                break;  // Customer found, exit loop
            }
        }

        if (customer == null) {
            System.out.println("Customer not found.");
            return; // Exit the method if the customer is not found
        }

        ArrayList<Product> boughtProducts = new ArrayList<>();
        boolean buy = true;

        while (buy) {
            System.out.println("Enter the number of the product you want to buy:");
            int productId = in.nextInt();
            in.nextLine();

            Product product = null;
            for (Product p : products) {
                if (p.getIdProduct() == productId) {
                    product = p;
                    break;  // Product found, exit loop
                }
            }

            if (product == null) {
                System.out.println("Product not found.");
            } else {
                boughtProducts.add(product);
            }

            System.out.println("Do you want to add another product to this order? (yes/no):");
            String response = in.nextLine();
            if (!response.equalsIgnoreCase("yes")) {
                buy = false;
            }
        }


        Transaction transaction = new Transaction(Transaction.getNextID(), LocalDate.now(), customer, boughtProducts);
        transactions.add(transaction);

        System.out.println("Purchase completed successfully!");
    }
    
    private void listProducts() {
        for (int i = 0; i < products.size(); i++) {
            System.out.println(products.get(i));
        }
    }
    
    private void listTransactions() {
        for (Transaction t : transactions) {
            System.out.println(t);
        }
    }


}
