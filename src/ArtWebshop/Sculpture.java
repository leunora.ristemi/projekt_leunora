package ArtWebshop;

public class Sculpture extends Product{
	

	public Sculpture(int idProduct, String name, double price) {
        super(idProduct, name, price);
    }
	
	private Material material;
	private boolean isOriginal;
	
	//Getters and Setters
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public boolean isOriginal() {
		return isOriginal;
	}
	public void setOriginal(boolean isOriginal) {
		this.isOriginal = isOriginal;
	}
	
	

}
