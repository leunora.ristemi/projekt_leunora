package PetDatei;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PetDModel {
	private final ObservableList<PetD> pets = FXCollections.observableArrayList();
    private final SimpleObjectProperty<PetD> petProperty = new SimpleObjectProperty<>();
    private SimpleBooleanProperty validNameProperty = new SimpleBooleanProperty(false);
    private final String FILE = "petsD.txt";
    private int nextId = 1;

    public ObservableList<PetD> getPetList() {
        return pets;
    }

    public SimpleObjectProperty<PetD> petProperty() {
        return petProperty;
    }

    public void savePet(String name, String species, String gender) {
        PetD.Species speciesEnum = PetD.Species.valueOf(species.toUpperCase());
        PetD.Gender genderEnum = PetD.Gender.valueOf(gender.toUpperCase());
        PetD pet = new PetD(nextId++, speciesEnum, genderEnum, name);
        pets.add(pet);
        petProperty.set(pet);
    }

    public void deletePet(PetD pet) {
        pets.remove(pet);
        if (!pets.isEmpty()) {
            petProperty.set(pets.get(0));
        } else {
            petProperty.set(null);
        }
    }

    public void loadPetsFromFile() {
        File file = new File(FILE);
        if (!file.exists()) {
            try {
                file.createNewFile(); // Leere Datei erstellen, wenn sie nicht existiert
            } catch (IOException e) {
                e.printStackTrace();
            }
            return; // Nichts weiter zu laden
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(",");
                if (parts.length == 4) {
                    int id = Integer.parseInt(parts[0]);
                    String name = parts[1];
                    PetD.Species species = PetD.Species.valueOf(parts[2]);
                    PetD.Gender gender = PetD.Gender.valueOf(parts[3]);
                    PetD pet = new PetD(id, species, gender, name);
                    pets.add(pet);
                    nextId = Math.max(nextId, id + 1);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void savePetsToFile() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(FILE))) {
            for (PetD pet : pets) {
                writer.write(pet.getID() + "," + pet.getName() + "," + pet.getSpecies() + "," + pet.getGender());
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public SimpleBooleanProperty validNameProperty() {
        return validNameProperty;
    }

    public void setValidName(boolean validName) {
        validNameProperty.set(validName);
    }
}