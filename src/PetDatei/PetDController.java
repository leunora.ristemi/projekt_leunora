package PetDatei;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.beans.binding.Bindings;

public class PetDController {
	private final PetDView view;
    private final PetDModel model;
    private final SimpleObjectProperty<PetD> currentPet = new SimpleObjectProperty<>();

    public PetDController(PetDModel model, PetDView view) {
        this.model = model;
        this.view = view;

        if (!model.getPetList().isEmpty()) {
            currentPet.set(model.getPetList().get(0));
            view.lwPet.getSelectionModel().selectFirst();
        }

        view.lwPet.getSelectionModel().selectedItemProperty().addListener((obs, oldPet, newPet) -> {
            currentPet.set(newPet);
            updateView(newPet);
        });

        view.btnDelete.setOnAction(this::delete);
        view.btnSave.setOnAction(this::save);
        
        view.btnSave.disableProperty().bind(view.txtName.textProperty().length().lessThan(2));
        view.btnDelete.disableProperty().bind(Bindings.isNull(currentPet));
        view.txtName.textProperty().addListener(this::updateNameValidity);
    }

    private void delete(ActionEvent event) {
        PetD selectedPet = view.lwPet.getSelectionModel().getSelectedItem();
        if (selectedPet != null) {
            model.deletePet(selectedPet);
        }
    }

    private void save(ActionEvent event) {
        String name = view.txtName.getText();
        String species = view.cmbSpecies.getValue().toString();
        String gender = view.cmbGender.getValue().toString();
        model.savePet(name, species, gender);
        view.clearFields();
    }

    private void updateView(PetD pet) {
        if (pet != null) {
            view.lblDataID.setText(String.valueOf(pet.getID()));
            view.lblDataName.setText(pet.getName());
            view.lblDataSpecies.setText(pet.getSpecies().toString());
            view.lblDataGender.setText(pet.getGender().toString());
        }
    }
    
    private void updateNameValidity(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		boolean valid = newValue.length() >= 4;
        model.setValidName(valid);
        view.txtName.getStyleClass().removeAll("valid", "invalid");
        if (valid) {
            view.txtName.getStyleClass().add("valid");
        } else {
            view.txtName.getStyleClass().add("invalid");
        }
    }
}