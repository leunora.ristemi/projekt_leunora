package PetDatei;

import javafx.application.Application;
import javafx.stage.Stage;

public class PetDMain extends Application {

    private PetDView view;
    private PetDModel model;
    private PetDController controller;

    @Override
    public void init() {
        model = new PetDModel();
        model.loadPetsFromFile();  
    }

    @Override
    public void start(Stage stage) {
        view = new PetDView(stage, model);
        controller = new PetDController(model, view);
        view.start();
    }

    @Override
    public void stop() {
        model.savePetsToFile();  
    }

    public static void main(String[] args) {
        launch();
    }
}
