package PetDatei;

public class PetD {
	public enum Species {CAT, DOG, HORSE};
	public enum Gender {MALE, FEMALE};
	
	private final int ID;
	private Species species;
	private Gender gender;
	private String name;
	
	public PetD(int ID, Species species, Gender gender, String name) {
		this.ID = ID;
		this.species = species;
		this.gender = gender;
		this.name = name;
	}
	
	// --- Getters and Setters ---

	public int getID() {
		return ID;
	}
	public String getName() {
        return name;
    }

    public Species getSpecies() {
        return species;
    }

    public Gender getGender() {
        return gender;
    }

    // Setters
    public void setName(String name) {
        this.name = name;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

	@Override
    public String toString() {
        return this.name + " (" + this.species + ", " + this.gender + ")";
    }

}
