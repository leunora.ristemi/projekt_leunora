package PetDatei;

import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class PetDView {
	private Stage stage;
    private PetDModel model;

    TextField txtName = new TextField();
    ComboBox<PetD.Species> cmbSpecies = new ComboBox<>();
    ComboBox<PetD.Gender> cmbGender = new ComboBox<>();
    Label lblDataID = new Label();
    Label lblDataName = new Label();
    Label lblDataSpecies = new Label();
    Label lblDataGender = new Label();
    ListView<PetD> lwPet;
    Button btnSave = new Button("Save");
    Button btnDelete = new Button("Delete");

    public PetDView(Stage stage, PetDModel model) {
        this.stage = stage;
        this.model = model;

        VBox root = new VBox();
        root.getChildren().addAll(createDataEntryPane(), createControlPane(), createDisplayPane());

        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource("PetD.css").toExternalForm());
        stage.setTitle("Your pets");
        stage.setScene(scene);
    }

    public void start() {
        stage.show();
    }

    private Pane createDataEntryPane() {
        GridPane pane = new GridPane();
        pane.setId("dataEntry");

        Label lblName = new Label("Name");
        Label lblSpecies = new Label("Species");
        Label lblGender = new Label("Gender");

        cmbSpecies.getItems().addAll(PetD.Species.values());
        cmbGender.getItems().addAll(PetD.Gender.values());

        pane.add(lblName, 0, 0);
        pane.add(txtName, 1, 0);
        pane.add(lblSpecies, 0, 1);
        pane.add(cmbSpecies, 1, 1);
        pane.add(lblGender, 0, 2);
        pane.add(cmbGender, 1, 2);

        return pane;
    }

    private Pane createControlPane() {
        GridPane pane = new GridPane();
        pane.setId("controlArea");

        pane.add(btnSave, 0, 0);
        pane.add(btnDelete, 1, 0);

        return pane;
    }

    private Pane createDisplayPane() {
        GridPane pane = new GridPane();
        pane.setId("listArea");
        lwPet = new ListView<>(model.getPetList());

        pane.add(lwPet, 0, 0);

        return pane;
    }

    public void updatePetList(ObservableList<PetD> petList) {
        lwPet.setItems(petList);
    }

    public void updateNameValidity(boolean valid) {
        txtName.getStyleClass().removeAll("valid", "invalid");
        txtName.getStyleClass().add(valid ? "valid" : "invalid");
    }

    public void clearFields() {
        txtName.clear();
        cmbSpecies.getSelectionModel().clearSelection();
        cmbGender.getSelectionModel().clearSelection();
    }
}