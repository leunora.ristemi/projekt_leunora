package Taschenrechner;

import javafx.application.Application;
import javafx.stage.Stage;

public class TaschenrechnerMain extends Application {
	
	private TaschenrechnerModel model;
	private TaschenrechnerView view;
	private TaschenrechnerController controller;
	
	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) {
		
		model = new TaschenrechnerModel();
		view = new TaschenrechnerView(primaryStage, model);
		controller = new TaschenrechnerController(model, view);
		
		view.start();
		
	}

}
