package Taschenrechner;

import java.util.ArrayList;
import java.util.List;

import PetDatei.PetD;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TaschenrechnerView {
	
	private final TaschenrechnerModel model;
	private final Stage primaryStage;
	private final VBox vbox;
	private final Label displayLabel;
	private final GridPane numbers;
	
	public TaschenrechnerView(Stage primaryStage, TaschenrechnerModel model) {
		
		this.primaryStage = primaryStage;
		this.model = model;
		
		vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(25);
        numbers = new GridPane();
        numbers.setAlignment(Pos.CENTER);
        numbers.setHgap(5);
        numbers.setVgap(5);

        Button[][] buttons = new Button[4][4];
        String[] buttonLabels = {"7", "8", "9", "+", "4", "5", "6", "-", "1", "2", "3", "=", "C", "0", "/", "*"};

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                Button button = new Button(buttonLabels[i * 4 + j]);
                button.setMinWidth(50);
                button.getStyleClass().add("button");
                buttons[i][j] = button;
                numbers.add(button, j, i);
            }
        }

        displayLabel = new Label();
        displayLabel.getStyleClass().add("display");

        vbox.getChildren().addAll(displayLabel, numbers);
        
        Scene scene = new Scene(vbox, 250,350);
        scene.getStylesheets().add(getClass().getResource("Taschenrechner.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setTitle("Taschenrechner");
		
	}
	
	public void start() {
		primaryStage.show();
	}

	public VBox getVbox() {
		return vbox;
	}
	
	public List<Button> getButtons() {
        List<Button> buttonList = new ArrayList<>();
        for (Node node : numbers.getChildren()) {
            if (node instanceof Button) {
                buttonList.add((Button) node);
            }
        }
        return buttonList;
    }
    
    public void setDisplayText(String text) {
        displayLabel.setText(text);
    }

    public void appendDisplayText(String text) {
        displayLabel.setText(displayLabel.getText() + text);
    }

    public String getDisplayText() {
        return displayLabel.getText();
    }

    public void clearDisplay() {
        displayLabel.setText("");
    }
}
