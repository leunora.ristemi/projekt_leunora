package Taschenrechner;

import java.util.List;

import javafx.scene.control.Button;

public class TaschenrechnerController {
	
	private final TaschenrechnerModel model;
	private final TaschenrechnerView view;
	private double zahl1 = 0;
    private double zahl2 = 0;
    private String operator = "";
    private boolean isTypingNumber = false;
	
	public TaschenrechnerController(TaschenrechnerModel model, TaschenrechnerView view){
		this.model = model;
		this.view = view;
		initialize();
	}
	
	private void initialize() {
        List<Button> buttons = view.getButtons();
        for (Button button : buttons) {
            button.setOnAction(e -> buttonClicked(button.getText()));
        }
    }
	
	private void buttonClicked(String buttonText) {
        if (buttonText.matches("[0-9]")) {
            if (!isTypingNumber) {
                view.setDisplayText(buttonText);
                isTypingNumber = true;
            } else {
                view.appendDisplayText(buttonText);
            }
        } else if (buttonText.equals("C")) {
            view.clearDisplay();
            zahl1 = 0;
            zahl2 = 0;
            operator = "";
            isTypingNumber = false;
        } else if (buttonText.equals("=")) {
            if (isTypingNumber) {
                zahl2 = Double.parseDouble(view.getDisplayText());
                double ergebnis = calculateResult();
                view.setDisplayText(Double.toString(ergebnis));
                zahl1 = ergebnis;
                isTypingNumber = false;
            }
        } else {
            if (isTypingNumber) {
                zahl1 = Double.parseDouble(view.getDisplayText());
                operator = buttonText;
                view.appendDisplayText(" " + operator + " ");
                isTypingNumber = false;
            }
        }
    }
	
	private double calculateResult() {
        switch (operator) {
            case "+":
                return TaschenrechnerModel.addieren(zahl1, zahl2);
            case "*":
                return TaschenrechnerModel.moltiplizieren(zahl1, zahl2);
            case "/":
            	return TaschenrechnerModel.dividieren(zahl1, zahl2);
            case "-":
            	return TaschenrechnerModel.subtrairen(zahl1,zahl2);
            default:
                return 0;
        }
    }
}