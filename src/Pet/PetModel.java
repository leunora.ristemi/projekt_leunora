package Pet;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PetModel {
private final SimpleObjectProperty<Pet> petProperty = new SimpleObjectProperty<>();
private SimpleBooleanProperty validNameProperty = new SimpleBooleanProperty(false);
private ObservableList<Pet> petList = FXCollections.observableArrayList();
private SimpleBooleanProperty petSavedProperty = new SimpleBooleanProperty(false); 
	
	public void savePet(Pet.Species species, Pet.Gender gender, String name) {
		Pet newPet = new Pet(species, gender, name);
		//new pet saved
        petProperty.set(newPet);
        petSavedProperty.set(true);
	}
	
	public void deletePet(Pet newPet) {
        petList.remove(newPet);
        petProperty.set(null);
        petSavedProperty.set(false);
    }
	
	public void addNewPet(Pet pet) {
        petList.add(pet);
        petSavedProperty.set(false);
    }
	
	public Pet getPet() {
		return petProperty.get();
	}
	
	public SimpleObjectProperty<Pet> petProperty() {
		return petProperty;
	}
	
	public SimpleBooleanProperty validNameProperty() {
        return validNameProperty;
    }

    public boolean isValidName() {
        return validNameProperty.get();
    }

    public void setValidName(boolean validName) {
        validNameProperty.set(validName);
    }
    
    public ObservableList<Pet> getPetList() {
        return petList;
    }
    
    public SimpleBooleanProperty petSavedProperty() {
        return petSavedProperty;
    }
    
}

