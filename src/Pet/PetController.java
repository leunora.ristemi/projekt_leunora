package Pet;

import Pet.Pet.Gender;
import Pet.Pet.Species;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

public class PetController {
	private PetView view;
	private PetModel model;
	private SimpleObjectProperty<Pet> currentPet = new SimpleObjectProperty<>();
	
	public PetController(PetModel model, PetView view) {
		this.model = model;
		this.view = view;
		
		view.btnDelete.disableProperty().bind(model.petProperty().isNull());
		view.txtName.textProperty().addListener(this::updateNameValidity);
		view.btnSave.disableProperty().bind(model.validNameProperty().not());
		view.btnAdd.disableProperty().bind(model.petSavedProperty().not()); 
		
		view.btnSave.setOnAction(this::save);
		view.btnDelete.setOnAction(this::delete);
		view.btnAdd.setOnAction(this::add);
        view.btnNext.setOnAction(this::next);
        view.btnPrevious.setOnAction(this::previous);
		
        model.petProperty().addListener((obs, oldPet, newPet) -> {
            updateView(newPet);
        });

        currentPet.addListener((obs, oldPet, newPet) -> {
            model.petProperty().set(newPet);
        });
	}
	// to save a new pet 
	private void save(ActionEvent e) {
		Species species = view.cmbSpecies.getValue();
		Gender gender = view.cmbGender.getValue();
		String name = view.txtName.getText();
		if (species != null && gender != null && name != null && name.length() > 0) {
			model.savePet(species, gender, name);
		}
	}
	// to delete of the display and list
	private void delete(ActionEvent e) {
        Pet petToDelete = currentPet.get();
        if (petToDelete != null) {
            model.deletePet(petToDelete);
            currentPet.set(null);
        }
    }
	// to add in the list
	private void add(ActionEvent e) {
        Pet petToAdd = currentPet.get();
        if(petToAdd != null) {
            model.addNewPet(petToAdd);
        }
    }
	// to see the elements of the list
	private void next(ActionEvent e) {
        ObservableList<Pet> petList = model.getPetList();
        int currentIndex = petList.indexOf(currentPet.get());
        if (currentIndex < petList.size() - 1) {
            Pet nextPet = petList.get(currentIndex + 1);
            currentPet.set(nextPet);
        }
    }
	
	private void previous(ActionEvent e) {
        ObservableList<Pet> petList = model.getPetList();
        int currentIndex = petList.indexOf(currentPet.get());
        if (currentIndex > 0) {
            Pet previousPet = petList.get(currentIndex - 1);
            currentPet.set(previousPet);
        }
    }
	
	private void updateView(Pet pet) {
        if (pet != null) {
            view.lblDataID.setText(Integer.toString(pet.getID()));
            view.lblDataName.setText(pet.getName());
            view.lblDataSpecies.setText(pet.getSpecies().toString());
            view.lblDataGender.setText(pet.getGender().toString());
            currentPet.set(pet); // Set the current pet
        } else {
            view.lblDataID.setText("");
            view.lblDataName.setText("");
            view.lblDataSpecies.setText("");
            view.lblDataGender.setText("");
            currentPet.set(null); // Reset the current pet
        }
        view.updatePetCount();
    }

	private void updateNameValidity(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        // Check if the name is valid 
		boolean valid = newValue.length() >= 2;
        model.setValidName(valid);
        view.txtName.getStyleClass().removeAll("valid", "invalid");
        if (valid) {
            view.txtName.getStyleClass().add("valid");
        } else {
            view.txtName.getStyleClass().add("invalid");
        }
    }

}

